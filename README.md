API Carrinho de Compras

Endpoints Disponiveis : 

/cupomDesconto - Cadastra o CRUD de Cupom de Desconto e faz algumas consultas

/produto - Cadastra o CRUD de Produto e faz algumas consultas

/carrinho - Realiza toda lógica do carrinho de compras.


Exemplo de Requisição Carrinho de Compras : 

{
	"produtos": [
		{
			"id":"1"
		},
		{
			"id":"1"
		},
		{
			"id":"1"
		},
		{
			"id":"1"
		},
		{
			"id":"2"
		}
	]
	,
	"discountCoupon": {
		"id":"1"
	}
}