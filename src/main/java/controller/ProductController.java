package controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.Product;
import repository.ProductReprository;

@RestController
@RequestMapping("/produto")
public class ProductController {
	
	private ProductReprository repository;
	
	public ProductController(ProductReprository productRepository) {
		this.repository = productRepository;
	}
	
	public ResponseEntity<Product> findById(@PathVariable long id){
		  return repository.findById(id)
		          .map(record -> ResponseEntity.ok().body(record))
		          .orElse(ResponseEntity.notFound().build());
		}
		@PostMapping
		public Product create(@RequestBody Product product){
		    return repository.save(product);
		}
		
		@PutMapping(value="/{id}")
		  public ResponseEntity<Product> update(@PathVariable("id") long id,
		                                        @RequestBody Product product){
		    return repository.findById(id)
		        .map(record -> {
		            record.setNome(product.getNome());
		            record.setValor(product.getValor());
		            Product updated = repository.save(record);
		            return ResponseEntity.ok().body(updated);
		        }).orElse(ResponseEntity.notFound().build());
		  }

	
		@GetMapping(value = "/pesquisaProduto/{name}")
		public ResponseEntity<List<Product>> findByNome(@PathVariable String name){

				List<Product> product = new ArrayList<Product>();
				product = repository.findByNome(name);
				
				return new ResponseEntity<List<Product>>(product,HttpStatus.OK);
			}

	
		@GetMapping(value = "/pesquisaValor/{valor}")
		public ResponseEntity<List<Product>> findByValor(@PathVariable String valor){

				List<Product> product = new ArrayList<Product>();
				Float valorConvertido =Float.valueOf(valor);
				product = repository.findByValor(valorConvertido);
				
				return new ResponseEntity<List<Product>>(product,HttpStatus.OK);
			}

	
	
	
	
	
}
