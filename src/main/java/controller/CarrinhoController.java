package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.Carrinho;
import model.DiscountCoupon;
import model.Product;
import repository.DiscountCouponRepository;
import repository.ProductReprository;

@RestController
@RequestMapping("/carrinho")
public class CarrinhoController {

	private ProductReprository productRepository;
	
	private DiscountCouponRepository discountRepository;
	
	
	public CarrinhoController(ProductReprository pRepository,DiscountCouponRepository dRepository) {
		productRepository = pRepository;
		discountRepository = dRepository;
	}
	
	
	
	@PostMapping
	public Carrinho create(@RequestBody Carrinho carrinho){
		Optional<DiscountCoupon> discountCoupon;
		DiscountCoupon discountCouponPresent = new DiscountCoupon();
		if(carrinho.getDiscountCoupon() != null) {
			discountCoupon = discountRepository.findById(carrinho.getDiscountCoupon().getId());	
			discountCouponPresent = discountCoupon.get();
		}
		Float valor = 0F;
		Float porcentagem = 0F;
		List<Product> products = new ArrayList<Product>();
		for (Product product : carrinho.getProdutos()) {
			
			Optional<Product> produto = productRepository.findById(product.getId());
			Product produtoPresent = produto.get();
		
			valor += produto.get().getValor();
			
			products.add(produtoPresent);
		
		}
		
		if(carrinho.getDiscountCoupon() != null) {
			porcentagem = (valor * discountCouponPresent.getValor()/100);
			valor = valor - porcentagem;
		}
		if(valor >= 1000 && valor < 5000) {
			porcentagem = (valor * 5/100);
			valor = valor - porcentagem;
		}
		if(valor >= 5000 && valor < 10000) {
			porcentagem = (valor * 7/100);
			valor = valor - porcentagem;
		}
		if(valor >= 10000) {
			porcentagem = (valor * 10/100);
			valor = valor - porcentagem;
		}
	
		carrinho.setTotal(valor);
		carrinho.setSubTotal(valor);
		carrinho.setProdutos(products);
		carrinho.setDiscountCoupon(discountCouponPresent);
		carrinho.setQuantidade(products.size());
		
		return carrinho;
	}
	
	
}
