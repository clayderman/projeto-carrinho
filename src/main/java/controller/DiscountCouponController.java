package controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.DiscountCoupon;
import repository.DiscountCouponRepository;

@RestController
@RequestMapping("/cupomDesconto")
public class DiscountCouponController {
		
	private DiscountCouponRepository repository;
	
	public DiscountCouponController(DiscountCouponRepository discountCouponRepository) {
		
		this.repository = discountCouponRepository;
		
	}
	
	@GetMapping(path = {"/{id}"})
	public ResponseEntity<DiscountCoupon> findById(@PathVariable long id){
	  return repository.findById(id)
	          .map(record -> ResponseEntity.ok().body(record))
	          .orElse(ResponseEntity.notFound().build());
	}
	@PostMapping
	public DiscountCoupon create(@RequestBody DiscountCoupon discountCoupon){
	    return repository.save(discountCoupon);
	}
	
	@PutMapping(value="/{id}")
	  public ResponseEntity<DiscountCoupon> update(@PathVariable("id") long id,
	                                        @RequestBody DiscountCoupon discountCoupon){
	    return repository.findById(id)
	        .map(record -> {
	            record.setNome(discountCoupon.getNome());
	            record.setValor(discountCoupon.getValor());
	            DiscountCoupon updated = repository.save(record);
	            return ResponseEntity.ok().body(updated);
	        }).orElse(ResponseEntity.notFound().build());
	  }
	
	@GetMapping(value = "/pesquisaCupom/{name}")
	public ResponseEntity<List<DiscountCoupon>> findByNome(@PathVariable String name){

			List<DiscountCoupon> discountCoupon = new ArrayList<DiscountCoupon>();
			discountCoupon = repository.findByNome(name);
			
			return new ResponseEntity<List<DiscountCoupon>>(discountCoupon,HttpStatus.OK);
		}
	
	@GetMapping(value = "/pesquisaValor/{estado}")
	public ResponseEntity<List<DiscountCoupon>> findByEstado(@PathVariable String valor){

			List<DiscountCoupon> discountCoupon = new ArrayList<DiscountCoupon>();
			Float valorConvertido =Float.valueOf(valor);
			discountCoupon = repository.findByValor(valorConvertido);
			
			return new ResponseEntity<List<DiscountCoupon>>(discountCoupon,HttpStatus.OK);
		}




}
