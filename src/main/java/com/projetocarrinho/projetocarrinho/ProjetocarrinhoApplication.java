package com.projetocarrinho.projetocarrinho;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("model")
@EnableJpaRepositories("repository")
@ComponentScan(basePackages={"controller"})
public class ProjetocarrinhoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetocarrinhoApplication.class, args);
	}

}
