package repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.Product;

@Repository
public interface ProductReprository extends JpaRepository<Product, Long>{

	public List<Product> findByNome(String nome);
	
	public List<Product> findByValor(Float valor);
	
	
}
