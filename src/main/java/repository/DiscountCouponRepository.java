package repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.DiscountCoupon;
import model.Product;

@Repository
public interface DiscountCouponRepository extends JpaRepository<DiscountCoupon, Long>{

	public List<DiscountCoupon> findByNome(String nome);
	
	public List<DiscountCoupon> findByValor(Float valor);

}
